package com.ggpredict.csgodemodownloader.models;

import java.math.BigInteger;

public class ShareCode {
    private BigInteger matchId;
    private BigInteger outcomeId; // reservation id
    private BigInteger tokenId; // tv port

    public ShareCode(BigInteger matchId, BigInteger outcomeId, BigInteger tokenId){
        this.matchId = matchId;
        this.outcomeId = outcomeId;
        this.tokenId = tokenId;

    }

    public BigInteger getMatchId() {
        return matchId;
    }

    public void setMatchId(BigInteger matchId) {
        if(matchId.longValue() < 1){
            return;
        }

        matchId = matchId;
    }

    public BigInteger getOutcomeId() {
        return outcomeId;
    }

    public void setOutcomeId(BigInteger outcomeId) {
        if(outcomeId.longValue() < 1){
            return;
        }

        outcomeId = outcomeId;
    }

    public BigInteger getTokenId() {
        return tokenId;
    }

    public void setTokenId(BigInteger tokenId) {
        if(tokenId.longValue() < 1){
            return;
        }

        tokenId = tokenId;
    }
}
