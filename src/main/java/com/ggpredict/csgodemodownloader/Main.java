package com.ggpredict.csgodemodownloader;

import com.ggpredict.csgodemodownloader.exceptions.InvalidShareCodeException;
import com.ggpredict.csgodemodownloader.models.ShareCode;
import com.ggpredict.csgodemodownloader.utils.ShareCodeDecoder;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args){

        try {
            ShareCode code = ShareCodeDecoder.decode("CSGO-6KYwq-atRfT-aVrKd-KHMwX-sPk9Q");

            BigInteger matchId = new BigInteger("3412832737692746494");
            BigInteger outcomeId = new BigInteger("3412839903845679294");
            BigInteger token = BigInteger.valueOf(33871);
            System.out.println(ShareCodeDecoder.encode(matchId, outcomeId, token));
        } catch (InvalidShareCodeException e) {
            e.printStackTrace();
        }

    }

}
