package com.ggpredict.csgodemodownloader.exceptions;

public class InvalidShareCodeException extends Exception {

    public InvalidShareCodeException(){
        super("Share code is not valid");
    }
}
