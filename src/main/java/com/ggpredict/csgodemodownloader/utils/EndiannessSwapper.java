package com.ggpredict.csgodemodownloader.utils;

import java.math.BigInteger;

public class EndiannessSwapper {

    public static BigInteger swapEndianness(BigInteger number){

        BigInteger result = new BigInteger("0");

        for(int i = 0; i < 144; i += 8) {
            BigInteger shiftedNumber = number.shiftRight(i).and(BigInteger.valueOf(255));
            result = result.shiftLeft(8).add(shiftedNumber);
        }

        return result;
    }

}
