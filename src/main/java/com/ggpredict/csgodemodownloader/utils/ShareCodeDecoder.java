package com.ggpredict.csgodemodownloader.utils;

import com.ggpredict.csgodemodownloader.exceptions.InvalidShareCodeException;
import com.ggpredict.csgodemodownloader.models.ShareCode;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShareCodeDecoder {

    // decoder and encoder used from https://github.com/akiver/CSGO-Demos-Manager/blob/7abb325ad3663732ca585addee52383a78751314/Core/ShareCode.cs

    private static final String DICTIONARY = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefhijkmnopqrstuvwxyz23456789";
    private static final String SHARECODE_PATTERN = "^CSGO(-?[\\w]{5}){5}$";
    private static final BigInteger BIT64MASK = new BigInteger("18446744073709551615");


    public static String encode(BigInteger matchId, BigInteger outcomeId, BigInteger token){

        BigInteger countedInt = token.shiftLeft(128).or(outcomeId.shiftLeft(64)).or(matchId);

        countedInt = EndiannessSwapper.swapEndianness(countedInt);

        String code = "";

        for (int i = 0; i < 25; i++){
            BigInteger[] modulo = countedInt.divideAndRemainder(BigInteger.valueOf(DICTIONARY.length()));
            countedInt = modulo[0];
            if(modulo.length < 2 || modulo[1].intValue() >= DICTIONARY.length()){
                return "";
            }

            code += DICTIONARY.charAt(modulo[1].intValue());
        }

        if(code.equals("")){
            return null;
        }

        String fullCode = String.format("CSGO-%s-%s-%s-%s-%s",code.substring(0,5), code.substring(5,10), code.substring(10,15), code.substring(15,20), code.substring(20));

        return fullCode;
    }


    public static ShareCode decode(String shareCode) throws InvalidShareCodeException {
        Pattern r = Pattern.compile(SHARECODE_PATTERN);

        Matcher m = r.matcher(shareCode);
        if (!m.find( )) {
            throw new InvalidShareCodeException();

        }

        String code = shareCode.replace("CSGO-","").replace("-", "");


        BigInteger counter = new BigInteger("0");


        for(int i = code.length() - 1; i >=0; i--){

            counter = counter.multiply(BigInteger.valueOf(DICTIONARY.length()));
            counter = counter.add(BigInteger.valueOf(DICTIONARY.indexOf(code.charAt(i))));
        }

        BigInteger unsignedInt = new BigInteger(ByteConverter.parseBigIntegerPositive(counter));

        BigInteger swappedInt = EndiannessSwapper.swapEndianness(unsignedInt);


        BigInteger matchId = swappedInt.and(BIT64MASK);
        BigInteger outcomeId = swappedInt.shiftRight(64).and(BIT64MASK);
        BigInteger token = swappedInt.shiftRight(128).and(BigInteger.valueOf(65535));



        System.out.println("MatchId: " + matchId);
        System.out.println("OutcomeId: " + outcomeId);
        System.out.println("token: " + token);

        ShareCode sc = new ShareCode(matchId, outcomeId, token);

        return sc;
    }
}
